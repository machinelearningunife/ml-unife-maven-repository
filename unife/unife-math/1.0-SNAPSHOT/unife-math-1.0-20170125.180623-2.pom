<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>unife</groupId>
    <artifactId>unife-math</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
    
    <!--This is the SCM information, which needs to be here so we can use the
    maven release plugin -->
    <scm>
        <url>https://github.com/AKSW/DL-Learner/</url>
        <connection>scm:git:git@bitbucket.org:machinelearningunife/unife-math.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:machinelearningunife/unife-math.git</developerConnection>
        <tag>${project.version}</tag>
    </scm>
    
    <licenses>
        <license>
            <name>GNU General Public License (GPL)</name>
            <url>http://www.gnu.org/licenses/gpl.txt</url>
        </license>
    </licenses>
    
    <organization>
        <name>MachineLearning@Unife - Department of Mathematics and Computer Science, 
            Department of Engineering - University of Ferrara</name>
        <url>https://sites.google.com/a/unife.it/ml/</url>
    </organization>
    
    <developers>
        <developer>
            <id>giuseta</id>
            <name>Giuseppe Cota</name>
            <email>giuseppe.cota@unife.it</email>
            <organization>MachineLearning@Unife - Department of Engineering - University of Ferrara</organization>
            <organizationUrl>https://sites.google.com/a/unife.it/ml/</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>1</timezone>
        </developer>
        <developer>
            <id>rzese</id>
            <name>Riccardo Zese</name>
            <email>riccardo.zese@unife.it</email>
            <organization>MachineLearning@Unife - Department of Engineering - University of Ferrara</organization>
            <organizationUrl>https://sites.google.com/a/unife.it/ml/</organizationUrl>
            <roles>
                <role>developer</role>
            </roles>
            <timezone>1</timezone>
        </developer>
    </developers>
    
    <build>
        <!-- extension used to deploy -->
        <extensions>
            <extension>
                <groupId>ar.com.synergian</groupId>
                <artifactId>wagon-git</artifactId>
                <version>0.2.5</version>
            </extension>
        </extensions>
    </build>
    
    <!-- Configuration for deployment (wagon-git) -->
    <pluginRepositories>
        <pluginRepository>
            <id>synergian-repo</id>
            <url>https://raw.github.com/synergian/wagon-git/releases</url>
        </pluginRepository>
    </pluginRepositories>
    
    <distributionManagement>
        <repository>
            <id>ml-unife-maven-repository</id>
            <name>ML@UniFe Maven Repository</name>
            <url>git:releases://git@bitbucket.org:machinelearningunife/ml-unife-maven-repository.git</url>
        </repository>
        <snapshotRepository>
            <id>ml-unife-maven-repository</id>
            <name>ML@UniFe Maven Repository</name>
            <url>git:snapshots://git@bitbucket.org:machinelearningunife/ml-unife-maven-repository.git</url>
        </snapshotRepository>
    </distributionManagement>
</project>